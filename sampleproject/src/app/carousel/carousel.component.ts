import { Component } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrl: './carousel.component.css'
})
export class CarouselComponent {
  bags = [
    {
      "imageUrl": "assets/bag1.jpeg",
      "bagName": "Stylish Traveler Backpack",
      "description": "A sleek and modern backpack designed for the stylish traveler. Features multiple compartments, padded straps for comfort, and a built-in USB charging port."
    },
    {
      "imageUrl": "assets/bag2.jpeg",
      "bagName": "Vintage Leather Messenger Bag",
      "description": "A classic messenger bag made from high-quality leather, perfect for the vintage enthusiast. Includes adjustable shoulder strap, multiple pockets, and a padded laptop compartment."
    },
    {
      "imageUrl": "assets/bag3.jpeg",
      "bagName": "Sporty Gym Duffel Bag",
      "description": "A spacious duffel bag designed for the active individual. Features a ventilated shoe compartment, water-resistant material, and padded handles for easy carrying."
    },
    {
      "imageUrl": "assets/bag4.jpg",
      "bagName": "Chic Crossbody Sling Bag",
      "description": "A trendy crossbody sling bag that combines fashion and functionality. Made from durable materials, with multiple pockets and an adjustable strap for versatility."
    },
    {
      "imageUrl": "assets/bag5.jpeg",
      "bagName": "Executive Briefcase",
      "description": "An elegant briefcase tailored for the modern executive. Crafted from premium materials, with a padded laptop sleeve, organizational pockets, and a sleek design."
    },
    {
      "imageUrl": "assets/bag6.jpeg",
      "bagName": "Outdoor Hiking Backpack",
      "description": "A rugged backpack built for outdoor adventures. Features adjustable straps, a hydration reservoir, and plenty of storage space for hiking essentials."
    },
    {
      "imageUrl": "assets/bag7.jpeg",
      "bagName": "Fashionable Tote Bag",
      "description": "A stylish tote bag perfect for everyday use. Made from durable materials with multiple compartments and a chic design."
    },
    {
      "imageUrl": "assets/bag8.jpeg",
      "bagName": "Weekend Travel Duffel",
      "description": "A versatile duffel bag ideal for weekend getaways. Features a spacious main compartment, shoe compartment, and sturdy handles for easy carrying."
    }
  ]

}
