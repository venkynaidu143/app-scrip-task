import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarouselComponent } from './carousel/carousel.component';
import { HeaderComponent } from './header/header.component';
import { CardsComponent } from './cards/cards.component';
import { FooterComponent } from './footer/footer.component';

const routes: Routes = [
  {path:'header',component:HeaderComponent},
  {path:'carousel',component:CarouselComponent},
  {path:'cards',component:CardsComponent},
  {path:'footer',component:FooterComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
